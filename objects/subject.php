<?php
class Subject{

    // database connection and table name
    private $conn;
    private $table_name = "subjects";

    // object properties
    public $id;
    public $name;
    public $total_question;

    public function __construct($db){
        $this->conn = $db;
    }

    // used by select drop-down list
    function index(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name . " ORDER BY name";

        $stmt = $this->conn->prepare( $query );

        $stmt->execute();

        return $stmt;
    }

    function store(){

        $query = "INSERT INTO " . $this->table_name . " SET name=:name, total_question=:total_question";

        $stmt = $this->conn->prepare($query);

        // posted values
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->total_question=htmlspecialchars($this->total_question);

        // to get time-stamp for 'created' field
       // $this->timestamp = date('Y-m-d H:i:s');

        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":total_question", $this->total_question);

        //$stmt->bindParam(":created", $this->timestamp);

        if($stmt->execute()){

            return true;

        }else{
            return false;
        }

    }
    function readOne(){

        $query = "SELECT * FROM  " . $this->table_name . " WHERE  id = ? LIMIT  0,1";

        $stmt = $this->conn->prepare( $query );

        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->name = $row['name'];
        $this->total_question = $row['total_question'];

    }
    function update(){

//        echo 'Updated';
        $query = "UPDATE
                " . $this->table_name . "
            SET
                name = :name, 
                total_question = :total_question
            WHERE
                id = :id";

        $stmt = $this->conn->prepare($query);


        // posted values
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->total_question = htmlspecialchars(strip_tags($this->total_question));
        $this->id = htmlspecialchars(strip_tags($this->id));


        // bind parameters
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':total_question', $this->total_question);
        $stmt->bindParam(':id', $this->id);


        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }



    }

    // delete the product
    function destroy($id){

        $query = "DELETE FROM " . $this->table_name . " WHERE id =".$id;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);

        if($result = $stmt->execute()){
            header("location:index.php");
//            $_SESSION["success"] = "Subject Deleted Successfully";
        }else{
            return false;
        }
    }

}
