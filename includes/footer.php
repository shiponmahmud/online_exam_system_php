
<footer class="footer">
		<span class="text-right">
		Copyright <a target="_blank" href="#">Your Website</a>
		</span>
    <span class="float-right">
		Powered by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a>
		</span>
</footer>

</div>
<!-- END main -->

<script src="<?php echo BASE_URL; ?>/assets/js/modernizr.min.js"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/moment.min.js"></script>

<script src="<?php echo BASE_URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo BASE_URL; ?>/assets/js/detect.js"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/fastclick.js"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/jquery.blockUI.js"></script>
<script src="<?php echo BASE_URL; ?>/assets/js/jquery.nicescroll.js"></script>

<!-- App js -->
<script src="<?php echo BASE_URL; ?>/assets/js/pikeadmin.js"></script>



</body>
</html>