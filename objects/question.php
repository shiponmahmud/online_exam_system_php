<?php
class Question{

    // database connection and table name
    private $conn;
    private $table_name = "questions";

    // object properties
    public $id;
    public $subject_id;
    public $question_decs;
    public $answer1;
    public $answer2;
    public $answer3;
    public $answer4;
    public $true_answer;
    public $created_at;
    public $updated_at;

    public function __construct($db){
        $this->conn = $db;
    }

    // used by select drop-down list
    function index(){
        //select all data

        $query = "SELECT * FROM questions INNER JOIN subjects ON questions.subject_id = subjects.id";

//        $query = "SELECT * FROM " . $this->table_name . " ORDER BY id";

        $stmt = $this->conn->prepare( $query );

        $stmt->execute();

        return $stmt;
    }

    function store(){

        $query = "INSERT INTO " . $this->table_name . " 
        SET 
        subject_id=:subject_id,
        question_decs=:question_decs,
        answer1=:answer1,
        answer2=:answer2,
        answer3=:answer3,
        answer4=:answer4,
        true_answer=:true_answer,
        created_at =:created_at,
        updated_at =:updated_at
        ";

        $stmt = $this->conn->prepare($query);

        // posted values
        $this->subject_id=htmlspecialchars(strip_tags($this->subject_id));
        $this->question_decs=htmlspecialchars(strip_tags($this->question_decs));
        $this->answer1=htmlspecialchars($this->answer1);
        $this->answer2=htmlspecialchars($this->answer2);
        $this->answer3=htmlspecialchars($this->answer3);
        $this->answer4=htmlspecialchars($this->answer4);
        $this->true_answer=htmlspecialchars($this->true_answer);
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');

        // to get time-stamp for 'created' field
       // $this->timestamp = date('Y-m-d H:i:s');

        // bind values
        $stmt->bindParam(":subject_id", $this->subject_id);
        $stmt->bindParam(":question_decs", $this->question_decs);
        $stmt->bindParam(":answer1", $this->answer1);
        $stmt->bindParam(":answer2", $this->answer2);
        $stmt->bindParam(":answer3", $this->answer3);
        $stmt->bindParam(":answer4", $this->answer4);
        $stmt->bindParam(":true_answer", $this->true_answer);
        $stmt->bindParam(":created_at", $this->created_at);
        $stmt->bindParam(":updated_at", $this->updated_at);

        //$stmt->bindParam(":created", $this->timestamp);

        if($stmt->execute()){

            return true;

        }else{
            return false;
        }

    }
    function readOne(){

        $query = "SELECT * FROM  " . $this->table_name . " WHERE  id = ? LIMIT  0,1";

        $stmt = $this->conn->prepare( $query );

        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->name = $row['name'];
        $this->total_question = $row['total_question'];

    }
    function update(){

//        echo 'Updated';
        $query = "UPDATE
                " . $this->table_name . "
            SET
                name = :name, 
                total_question = :total_question
            WHERE
                id = :id";

        $stmt = $this->conn->prepare($query);


        // posted values
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->total_question = htmlspecialchars(strip_tags($this->total_question));
        $this->id = htmlspecialchars(strip_tags($this->id));


        // bind parameters
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':total_question', $this->total_question);
        $stmt->bindParam(':id', $this->id);


        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }



    }

    // delete the product
    function destroy($id){

        $query = "DELETE FROM " . $this->table_name . " WHERE id =".$id;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);

        if($result = $stmt->execute()){
            header("location:index.php");
//            $_SESSION["success"] = "Subject Deleted Successfully";
        }else{
            return false;
        }
    }

}
