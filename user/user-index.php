
<!--Header Start  -->
<?php include('../includes/header.php') ?>
<!--Header End  -->

 
	<!-- Left Sidebar Start-->
    <?php include('../includes/left-sidebar.php') ?>
    <!-- Left Sidebar End-->

    <!-- Start  content-page -->

<div class="content-page">

    <!-- Start content -->
    <div class="content">

        <div class="container-fluid">


            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Blank Page</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active">Blank Page</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xl-12">
                    Content here
                </div>
            </div>



        </div>
        <!-- END container-fluid -->

    </div>
    <!-- END content -->

</div>
	<!-- END content-page -->

<!-- Start Footer -->
 <?php include('../includes/footer.php') ?>
<!-- End Footer -->

<!-- BEGIN Java Script for this page -->

<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

<script>
    $(document).ready(function() {
        // data-tables
        $('#example1').DataTable();

        // counter-up
        $('.counter').counterUp({
            delay: 10,
            time: 600
        });
    } );
</script>
	
