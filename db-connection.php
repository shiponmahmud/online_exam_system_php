<?php
class Database{
    private $host = "localhost";
    private $db_name = "online_exam_oop";
    private $username = "root";
    private $password = "";
    public $conn;

    // get the database connection
    public function getDbConnection(){

        $this->conn = null;

        try{
            $this->conn = new PDO("mysql:host=". $this->host.";dbname=".$this->db_name, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}

//$db = new Database;
//$db->getDbConnection();

