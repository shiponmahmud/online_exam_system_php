<div class="left main-sidebar">

    <div class="sidebar-inner leftscroll">

        <div id="sidebar-menu">
            <ul>
                <li class="submenu">
                    <a class="active" href="<?php echo BASE_URL; ?>index.php"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                </li>

                <li class="submenu">
                    <a class="" href="<?php echo BASE_URL; ?>subject/index.php"><i class="fa fa-fw fa-bars"></i><span> Subject </span> </a>
                </li>

                <li class="submenu">
                    <a href="#"><i class="fa fa-fw fa-table"></i> <span> Users </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo BASE_URL; ?>user/user-index.php">Add User</a></li>
                    </ul>
                </li>




                <li class="submenu">
                    <a href="#"><i class="fa fa-fw fa-table"></i> <span> Question </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo BASE_URL; ?>question/index.php">Add Question</a></li>
                    </ul>
                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

    </div>

</div>
