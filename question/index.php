<?php
session_start();
// include database and object files
include_once '../db-connection.php';
include_once '../objects/question.php';
include_once '../objects/subject.php';
$database = new Database();
$db = $database->getDbConnection();
// pass connection to objects
$question = new Question($db);
$subject = new Subject($db);
if(isset($_GET['id']) ){
    $delId = $_GET['id'] ;
    $subject->destroy($delId);
}



// Get subject
$sm = $subject->index();
$stmt = $question->index();
$num = $stmt->rowCount();


?>
<!--Header Start  -->
<?php include('../includes/header.php') ?>
<!--Header End  -->

 
	<!-- Left Sidebar Start-->
    <?php include('../includes/left-sidebar.php') ?>
    <!-- Left Sidebar End-->

    <!-- Start  content-page -->

<div class="content-page">

    <!-- Start content -->
    <div class="content">

        <div class="container-fluid">


            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Question</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active">Question</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h3><i class="fa fa-check-square-o"></i>ADD Question</h3>
                        </div>

                        <div class="card-body">

                            <?php
                            if(isset($_SESSION["success"])){
                                echo "<div class='alert alert-success'>".$_SESSION['success']."</div>";

                                session_destroy();

                            }
                            // if the form was submitted - PHP OOP CRUD Tutorial
                            if($_POST){

                                // set Subject property values
                                $question->subject_id = $_POST['subject_id'];
                                $question->question_decs = $_POST['question_decs'];
                                $question->answer1 = $_POST['answer1'];
                                $question->answer2 = $_POST['answer2'];
                                $question->answer3 = $_POST['answer3'];
                                $question->answer4 = $_POST['answer4'];
                                $question->true_answer = $_POST['true_answer'];

                                // create the Question
                                if($question->store()){
                                    echo'<meta http-equiv="refresh" content="2">';
                                    echo "<div class='alert alert-success'>Question was created.</div>";
                                   // header('Location: ' . $_SERVER['HTTP_REFERER']);
                                }else{
                                    echo "<div class='alert alert-danger'>Unable to create Question.</div>";
                                }

                            }
                            ?>

                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" autocomplete="off">

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Subject:</label>
                                    <div class="col-sm-8">
                                        <select class='form-control' name='subject_id'>
                                            <option>Select Subject...</option>
                                            <?php
                                            while ($row_subject = $sm->fetch(PDO::FETCH_ASSOC)){
                                                extract($row_subject);
                                                ?>
                                                <option value='<?= $id ?>'><?= $name ?></option>;
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Description:</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="question_decs" class="form-control" id="" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Answer 1:</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="answer1" class="form-control" id="" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Answer 2:</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="answer2" class="form-control" id="" placeholder="" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Answer 3:</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="answer3" class="form-control" id="" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Answer 4:</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="answer4" class="form-control" id="" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">True Answer:</label>
                                    <div class="col-sm-8">
                                        <input type="text"  name="true_answer" class="form-control" id="" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">SAVE</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h3><i class="fa fa-table"></i> VIEW SUBJECT</h3>
                        </div>

                        <div class="card-body">

                            <table class="table table-responsive-xl">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Question</th>
                                    <th scope="col"> Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($num>0) {
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                        extract($row);
//                                        var_dump($row);


                                        ?>
                                <tr>
                                    <td><?= $row['name'] ?></td>
                                    <td><?=
                                        $row['question_decs']

                                        ?></td>

                                    <td>
                                        <a href='edit.php?id=<?= $row['id'];?>' class='btn btn-info left-margin'>
                                            <span class='glyphicon glyphicon-edit'></span> Edit
                                        </a>

<!--                                        <a delete-id='{$id}' class='btn btn-danger delete-object'class='btn btn-danger delete-object'>-->
<!--                                            <span class='glyphicon glyphicon-remove'></span> Delete-->
<!--                                        </a>-->

                                        <a class='btn btn-danger delete-object' href='index.php?id=<?= $row['id'];?>' >Delete</a>
                                    </td>
                                </tr>
                                <?php }}?>
                                </tbody>
                            </table>

                        </div>
                    </div><!-- end card-->
                </div>
            </div>



        </div>
        <!-- END container-fluid -->

    </div>
    <!-- END content -->

</div>
	<!-- END content-page -->

<!-- Start Footer -->
 <?php include('../includes/footer.php') ?>
<!-- End Footer -->

<!-- BEGIN Java Script for this page -->

<script src="<?php echo BASE_URL; ?>assets/plugins/counterup/jquery.counterup.min.js"></script>

<script>
    $(document).ready(function() {
        // data-tables
        $('#example1').DataTable();

        // counter-up
        $('.counter').counterUp({
            delay: 10,
            time: 600
        });
    } );
</script>
	
